Dezimal <-> Binär

Netzwerktools
- ping
- tracert

OSI-Schichten
- Geräte
- Adressen

Lankabel
- Für was?
- länge?

Netzwerktopologien

Raid?

IP-Klassen
- A, B, C
- Private Bereiche
- Apipa

Gültige IP?

Subnetmasken
- Dezimal
- Binär
- /

Anzahl hosts in Subnet

Kommunizierbar in Subnet?

Freigabe/Sicherheit
- Unterschiede
- Versteckte
