# Vor- und Nachteile von Topologien

## Ring

Ring-Topology used to be used really often, it can have redundancy if one node fails.

Throughput can go down quite a bit because it needs to go through a lot of nodes -> slower and less responsive

## Vermascht - (SPOF)

Has redundancy, not completely though, speed varies with your location in the network

## Star - SPOF!!

Central point of failure, I don't like it. Can be centrally managed which can be useful for companies

## Vollvermascht

Fastest of the bunch, direct connection to everyone, in practice not used often because it requires A LOT of cables

## Line - SPOF

Just don't, slow, a lot of routing and maybe

## Tree - SPOF

Speed varies A LOT on where you want to connect to and where you are, but it will be really fast with the machines near you. If one machine fails, it can either not break anything or break everything (if root breaks)

## Bus

Efficient cable usage, low throughput, low security because everyone can listen in on every package sent, I don't like it either