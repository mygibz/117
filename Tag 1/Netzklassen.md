# Netzklassen

## Class A

- 0-127 (255.0.0.0)
- 10.0.0.0 until 10.255.255.255
- Loopback 127.0.0.1

## Class B

- 128-191 (255.255.0.0) 
- 172.16.0.0 until 172.31.255.255
- Apipa 169.254.x.x (automatic private IP addressing, used if DHCP server fails)

## Class D

- 192-223 (255.255.255.0)
- 192.168.0.0 until 192.168.255.255