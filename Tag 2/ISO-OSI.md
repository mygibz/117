# ISO/OSI

|    Nr | English            | German                 | TCP/IP Modell | Devices                 | Test Cases            |
| ----: | ------------------ | :--------------------- | :------------ | :---------------------- | --------------------- |
| ~~8~~ | ~~UserLayer~~      | ~~Nutzerschicht~~      | ~~USER~~      | ~~Stupidity~~           |                       |
|     7 | Application Layer  | Anwendungsschicht      | Application   |                         |                       |
|     6 | Presentation Layer | Darstellungsschicht    | Application   |                         |                       |
|     5 | Session Layer      | Sitzungsschicht        | Application   |                         |                       |
|     4 | Transport Layer    | Transportschicht       | Transport     | **Port**                |                       |
|     3 | Network Layer      | Vermittlinggschicht    | Internet      | Router (**IP**)         | ip a / tracert / ping |
|     2 | Data Link Layer    | Sicherungsschicht      | Link          | Switch/Bridge (**MAC**) | ping loopback         |
|     1 | Physical Layer     | Bitübertragungsschicht | Link          | Repeater/HUB            | arp table             |